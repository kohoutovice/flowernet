import os
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--source", action="store", type=str, help="")
parser.add_argument("--dest", action="store", type=str, help="")
args = parser.parse_args()

with open("lat_species.txt") as file:
    for i, specie in enumerate(file.read().splitlines()):
        master = specie.split(" ")[0]
        p = args.dest.rstrip("/") + '/' + master
        if not os.path.isdir(p):
            os.makedirs(p)
        command = 'mv ' + args.source.rstrip("/") + '/' + str(i) +  '/* '  +  args.dest.rstrip("/") + '/' + master
        print(command)
        #subprocess.call(command ,shell=True)
